# Windows

Sequence to install on Windows

## pycairo

Download pycairo from https://www.lfd.uci.edu/~gohlke/pythonlibs/. Use the version pycairo‑1.18.2‑cp35‑cp35m‑win_amd64.whl

`python3 -m pip install pycairo-1.18.2-cp35-cp35m-win_amd64.whl`

## all the other libraries

`python3 -m pip install -r requirements.txt`